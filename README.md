This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

## Disclaimer

Few of the lines in the bitbucket-pipeline.yml file are commented out. Uncomment them once you have the test cases ready and have configured the Environment Variables accordingly.

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Dockerfile

Dockerfile written here is to containerize the React App. 

You can learn more in the [Write Dockerfile](https://docs.docker.com/get-started/part2/).

### task-definition.json

This json file has information about the services to bring up and resources allocated to them. 

You can learn more in the [Write task-definition](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create-task-definition.html)

### Integration and Deployment

Implement CI/CD from the scratch from [CI/CD Using Bitbucket's Pipeline and AWS's Elastic Container Service (ECS)](https://medium.com/@darshankodipalli/ci-cd-using-bitbuckets-pipeline-and-aws-s-elastic-container-service-ecs-a7ddb575a680)

